<?php

namespace PackageBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @var string
     */
    private $alias;

    /**
     * @param string $alias
     */
    public function __construct($alias)
    {
        $this->alias = $alias;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $this->fillBundleAttributes($treeBuilder->root($this->alias)->children());

        return $treeBuilder;
    }

    /**
     * @param NodeBuilder $nodeBuilder
     * @return void
     */
    private function fillBundleAttributes(NodeBuilder $nodeBuilder)
    {
        $nodeBuilder
            ->scalarNode('base_url')
                ->defaultValue('')
            ->end()
        ;
    }
}
